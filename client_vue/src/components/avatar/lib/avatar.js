export default {
	name: "avatar",
	data(vm) {
		return {
		};
	},
	components: {},
	methods: {
	},
	props: {
		nom: {
			type: String,
			default: "John Doe"
		},
		profession: {
			type: String,
			default: "Web Developer"
		}
	}
};