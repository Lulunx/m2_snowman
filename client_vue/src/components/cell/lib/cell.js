export default {
    data() {
        return {

        }
    },
    computed: {
    },
    methods: {

    },
    props: {
        cell: {
            type: Object,
            default: {
                id: -5
            }
        },
        specialCells: {
            type: Object,
            default: {
                player: null,
                west: null,
                east: null,
                north: null,
                south: null,
                littleAndMediumSnow:null,
                mediumAndBigSnow:null,
                finalSnow:null
            }
        }
    },
    mounted() {

    }
}