import Cell from "../../cell/cell.vue"
export default {
    data() {
        return {
            cells: {},
            moving: false,
            specialCells: {
                player: null,
                west: null,
                east: null,
                north: null,
                south: null,
                finalSnow: -1
            }
        }
    },
    components: {
        Cell
    },
    computed: {
        url()
        {
            return this.$store.state.url;
        } 
    },
    methods: {
        //Récupère le plateau de jeu au lancement de la page
        request() {
            fetch(this.url + "cells").then(r => r.json()).then(data => {

                this.cells = data;

                let table = document.createElement("div");
                table.setAttribute("id", "table");

                this.cells.forEach(cell => {
                    cell.id = cell.cell.value.split("#")[1];
                });
                this.getPlacements();
            })
        },
        //Récupère les cellules spéciales au lancement de la page
        getPlacements() {
            fetch(this.url + "cellArroundPlayer").then(r => r.json()).then(data => {
                this.makeSpecialCells(data[0]);

            });
        },
        //Affiche la fenêtre pour prévenir l'utilisateur que le déplacement est impossible
        impossibleMove() {
            this.$store.commit("updateSnackMessage", "Déplacement impossible");
            this.$store.commit("setSnackColor", "warning");
            this.$store.commit("showSnack", true);
        },
        //Lance le processus du déplacement du joueur
        move(where) {
            //Si aucun déplacement en cours
            if (!this.moving) {
                let self = this;
                this.moving = true;
                //On lance la requete
                fetch(this.url + "move?dest=" + where).then(r => r.json()).then(data => {
                    //Si pas de données, c'est que déplacement impossible
                    if (!data) {
                        self.impossibleMove();
                        self.moving = false;
                    }
                    //Sinon on a récupéré les cellules spéciales -> On met à jour
                    else {
                        self.makeSpecialCells(data[0]);
                        self.moving = false;
                    }
                });
            }
        },
        //Evenement sur clique du bouton Reset -> Réinitialisation du plateau de jeu
        resetGame() {
            let self = this;
            fetch(this.url + "resetGame").then(r => r.json()).then(data => {
                self.makeSpecialCells(data[0]);
                this.$store.commit("showFirework", false);
            });
        },
        //Fonction permettant de mettre a jour le tableau de cellules spéciales -> Si on a l'info on la récupère sinon null
        makeSpecialCells(data) {
            this.specialCells.player = data["cellPlayer"] ? data["cellPlayer"].value.split("#")[1] : null;

            this.specialCells.east = data["cellEast"] ? data["cellEast"].value.split("#")[1] : null;
            this.specialCells.west = data["cellWest"] ? data["cellWest"].value.split("#")[1] : null;
            this.specialCells.north = data["cellNorth"] ? data["cellNorth"].value.split("#")[1] : null;
            this.specialCells.south = data["cellSouth"] ? data["cellSouth"].value.split("#")[1] : null;

            this.specialCells.littleSnow = data["littleSnow"] ? data["littleSnow"].value.split("#")[1] : null;
            this.specialCells.mediumSnow = data["mediumSnow"] ? data["mediumSnow"].value.split("#")[1] : null;
            this.specialCells.bigSnow = data["bigSnow"] ? data["bigSnow"].value.split("#")[1] : null;

            this.specialCells.littleAndMediumSnow = data["littleAndMediumSnow"] ? data["littleAndMediumSnow"].value.split("#")[1] : null;
            this.specialCells.mediumAndBigSnow = data["mediumAndBigSnow"] ? data["mediumAndBigSnow"].value.split("#")[1] : null;

            const saveFinalSnow = this.specialCells.finalSnow
            this.specialCells.finalSnow = data["finalSnow"] ? data["finalSnow"].value.split("#")[1] : null;
            this.checkIfEndGame(saveFinalSnow, this.specialCells.finalSnow);
        },
        //Vérification de fin de partie
        checkIfEndGame(oldVar, newVar){
            if(oldVar == null && newVar != null)
            {
                this.$store.commit("showFirework", true);
                this.$store.commit("updateSnackMessage", "Félicitations");
                this.$store.commit("setSnackColor", "success");
                this.$store.commit("showSnack", true);
            }
        }
    },
    mounted() {
        this.request();
        //Surveillance des appuies sur les fleches directionnelles
        window.addEventListener('keyup', (e) => {
            const key = e.which || e.keyCode;
            switch (key) {
                case 38:
                    this.move("up");
                    break;
                case 40:
                    this.move("down");
                    break;
                case 37:
                    this.move("left");
                    break;
                case 39:
                    this.move("right");
                    break;
            }
        })
    }
}