import Plateau from "../../../components/plateau/plateau.vue"
import Firework from "../../../components/firework/firework.vue"
export default {
    name: "home",
    components: {
        Plateau,
        Firework
    },
    methods: {
        test: function () {
            this.$store.commit("updateSnackMessage", "Bonjour");
            this.$store.commit("setSnackColor", "error");
            this.$store.commit("showSnack", true);
        }
    },
    computed:{
        showFirework(){
            return this.$store.state.fireworkShow;
        }
    }
};