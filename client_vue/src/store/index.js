import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		snackMessage: '',
		snackShow: false,
		fireworkShow: false,
		snackColor: "success",
		loading: false,
		url: 'http://localhost:8085/'
	},
	mutations: {
		updateSnackMessage: (state, msg) => {
			state.snackMessage = msg;
		},

		showSnack: (state, status) => {
			state.snackShow = status;
		},

		showFirework: (state, status) => {
			state.fireworkShow = status;
		},

		setSnackColor: (state, color) => {
			state.snackColor = color;
		},

		updateLoading: (state, value) => {
			state.loading = value;
		}
	},
	actions: {
		
	}
})
