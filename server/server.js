const { Connection, query } = require('stardog');
let express = require('express');
let app = express();

let fs = require('fs');
let url = require('url');
let cors = require('cors');

//Autorisation des requetes cross origine
app.use(cors());

//Paramètre de l'API Stardog
const conn = new Connection({
    username: 'admin',
    password: 'admin',
    endpoint: 'http://192.168.1.37:5820',
});

//Requête qui retourne les cellules spéciales ( Utilisée à plusieurs endroits, donc set en dur ici )
const specialCellsReq = `
SELECT ?cellPlayer ?cellNorth ?cellEast ?cellWest ?cellSouth ?littleSnow ?mediumSnow ?bigSnow ?littleAndMediumSnow ?mediumAndBigSnow ?finalSnow WHERE{ 
    ?cellPlayer rdf:type :CellPlayer .
    ?littleSnow rdf:type :littleSnowman .
    ?mediumSnow rdf:type :mediumSnowman .
    ?bigSnow rdf:type :bigSnowman .
    OPTIONAL {?cellNorth rdf:type :CellNorthPlayer  }. 
    OPTIONAL {?cellEast rdf:type :CellEastPlayer } . 
    OPTIONAL {?cellWest rdf:type :CellWestPlayer } . 
    OPTIONAL {?cellSouth rdf:type :CellSouthPlayer} .  
    OPTIONAL {?littleAndMediumSnow rdf:type :littleAndMediumSnowman} .  
    OPTIONAL {?mediumAndBigSnow rdf:type :mediumAndBigSnowman} .  
    OPTIONAL {?finalSnow rdf:type :finalSnowman} .  
  }`;


//Fonction générique pour faire une requête sur Stardog
//queryText la requete SPARQL
//callback la fonction a exécuter après exécution de la rq
function makeStardogRequest(queryText, callback, limit = 9999, offset = 0) {
    query.execute(conn, 'snowman', queryText, 'application/sparql-results+json', {
        limit: limit,
        offset: offset,
        reasoning: true
    }).then(({ body }) => {
        //console.log(body)
        callback(body);
    });
}

//Retourne au client la liste des cellules du plateau ( au chargement de la page )
app.get('/cells', function (req, res) {
    makeStardogRequest(`select ?cell ?x ?y 
    where 
    {
         ?cell rdf:type :Cell . 
         ?cell :x ?x .
         ?cell :y ?y
    }
    ORDER BY ?x ?y`
        , function (data) {
            res.send(data.results.bindings);
        });
});

//Retourne au client les cellules spéciales ( au chargement de la page )
app.get('/cellArroundPlayer', function (req, res) {
    makeStardogRequest(specialCellsReq, function (data) {
        res.send(data.results.bindings);
    });
});

//Permet de réinitialiser la partie, les positions sont aléatoires
//Retourne les cellules spéciales
app.get('/resetGame', function (req, res) {
    makeStardogRequest(`
    DELETE {
        ?player :isOn ?playerOldCell .
        ?littleSnow :isOn ?littleSnowOldCell .
        ?mediumSnow :isOn ?mediumSnowOldCell .
        ?bigSnow :isOn ?bigSnowOldCell .
    }
    INSERT {
        ?player :isOn ?playerNewCell . 
        ?littleSnow :isOn ?littleSnowNewCell .
        ?mediumSnow :isOn ?mediumSnowNewCell .
        ?bigSnow :isOn ?bigSnowNewCell .
    }
    WHERE {
        ?player rdf:type :Player .
        ?playerOldCell rdf:type :CellPlayer .
        ?playerNewCell :x `+ (Math.floor(Math.random() * Math.floor(9)) + 1) + ` .
        ?playerNewCell :y `+ (Math.floor(Math.random() * Math.floor(9)) + 1) + ` .

        ?littleSnow rdfs:label 'littleSnowman' .
        ?littleSnowOldCell rdf:type :littleSnowman .
        ?littleSnowNewCell :x `+ (Math.floor(Math.random() * Math.floor(8)) + 1) + ` .
        ?littleSnowNewCell :y `+ (Math.floor(Math.random() * Math.floor(8)) + 1) + ` .

        ?mediumSnow rdfs:label 'mediumSnowman' .
        ?mediumSnowOldCell rdf:type :mediumSnowman .
        ?mediumSnowNewCell :x `+ (Math.floor(Math.random() * Math.floor(8)) + 1) + ` .
        ?mediumSnowNewCell :y `+ (Math.floor(Math.random() * Math.floor(8)) + 1) + ` .

        ?bigSnow rdfs:label 'bigSnowman' .
        ?bigSnowOldCell rdf:type :bigSnowman .
        ?bigSnowNewCell :x `+ (Math.floor(Math.random() * Math.floor(8)) + 1) + ` .
        ?bigSnowNewCell :y `+ (Math.floor(Math.random() * Math.floor(8)) + 1) + ` .
    }
    `, function (data) {
        makeStardogRequest(specialCellsReq, function (data) {
            res.send(data.results.bindings);
        });

    });
});

//Permet d'effectuer un déplacement
//paramètre : dest=[up,down,right,left] en fonction de la direction
//retourne les cellules spéciales ou false si déplacement non autorisé
app.get('/move', function (req, res) {
    switch (req.query.dest) {
        case "up":
            cellToCheck = ":CellNorthPlayer";
            move = ":hasNorth";
            break;
        case "down":
            cellToCheck = ":CellSouthPlayer";
            move = ":hasSouth";
            break;
        case "right":
            cellToCheck = ":CellEastPlayer";
            move = ":hasEast";
            break;
        case "left":
            cellToCheck = ":CellWestPlayer";
            move = ":hasWest";
            break;
    }
    //On check si la cellule dans la direction existe bien ( que l'utilisateur ne triche pas )
    makeStardogRequest(`ASK WHERE { ?cell rdf:type ` + cellToCheck + `}`, function (data) {
        if (data.boolean) {
            //On récupère le type exact de la boule
            makeStardogRequest(`SELECT ?snowMovable WHERE { 
                ?cellPlayer rdf:type :CellPlayer .
                ?cellPlayer ` + move + ` ?cellMove .
                ?cellMove rdf:type ?snowMovable .
                FILTER(?snowMovable IN (:littleSnowman, :mediumSnowman, :bigSnowman, :mediumAndBigSnowman, :littleAndMediumSnowman, :finalSnowman))
              }`
                , function (data) {
                    //Si il y'a une boule
                    if (data.results.bindings.length != 0 ) {
                        
                                let cellVal = data.results.bindings[0].snowMovable.value.split("#")[1]
                                //Si il y a plusieurs boules sur la cellule c'est qu'on essaye de bouger
                                // :mediumAndBigSnowman, :littleAndMediumSnowman, :finalSnowman
                                //-> Interdit, on bouge pas
                                if (data.results.bindings.length > 1) {
                                    res.send(false);
                                    return;
                                }
                                //On switch en fonction du résultat pour autoriser les cellules ou pas
                                let cellNearMove, rdfType, rdfLabel
                                switch (cellVal) {
                                    case "littleSnowman":
                                        cellNearMove = ":mediumSnowman, :mediumAndBigSnowman";
                                        rdfType = ":littleSnowman";
                                        rdfLabel = "littleSnowman";
                                        break;
                                    case "mediumSnowman":
                                        cellNearMove = ":bigSnowman";
                                        rdfType = ":mediumSnowman";
                                        rdfLabel = "mediumSnowman";
                                        break;
                                    case "bigSnowman":
                                        cellNearMove = "";
                                        rdfType = ":bigSnowman";
                                        rdfLabel = "bigSnowman";
                                        break;
                                    default:
                                        res.send(false);                        //Si ce n'est pas une boule basique, ca ne bouge pas
                                        return;                                 //(Par précaution, on ne doit jamais passer ici)
                                }
                                //On check que le déplacament est possible pour la boule
                                makeStardogRequest(`

                                ASK {
                                    ?cellPlayer rdf:type :CellPlayer .
                                    ?cellPlayer ` + move + ` ?cellMove .
                                    ?cellMove rdf:type ` + rdfType + ` .
                                    ?cellMove ` + move + ` ?cellNearMove .
                                    {
                                      ?cellNearMove rdf:type :Snowman .
                                      ?cellNearMove :hasOnHim ?boule .
                                      ?cellMove rdf:type :CellSouthPlayer .
                                      FILTER(?boule IN (` + cellNearMove + `))
                                    }
                                    UNION
                                    {
                                      ?cellNearMove rdf:type :Cell .
                                      filter not exists { ?cellNearMove rdf:type :Snowman }.
                                      filter not exists { ?cellNearMove rdf:type :Wall }
                                    }
                                  }`
                                    , function (data) {
                                        //Si déplacement possible On bouge la boule et le joueur
                                        if (data.boolean) {
                                            makeStardogRequest(`
                                        DELETE { 
                                            ?player :isOn ?oldCellPlayer .
                                            ?snow   :isOn ?oldCellSnow .
                                        }
                                        INSERT {
                                            ?player :isOn ?newCellPlayer .
                                            ?snow :isOn ?newCellSnow .
                                        }
                                        WHERE {
                                            ?player rdf:type :Player .
                                            ?oldCellPlayer rdf:type :CellPlayer .
                                            ?oldCellPlayer ` + move + ` ?newCellPlayer .
                                            
                                            ?snow rdfs:label  '` + rdfLabel + `'.
                                            ?oldCellSnow rdf:type ` + rdfType + ` .
                                            ?oldCellSnow ` + move + ` ?newCellSnow .
                                        }`
                                                , function (data) {
                                                    makeStardogRequest(specialCellsReq, function (data) {
                                                        res.send(data.results.bindings);
                                                    });
                                                });
                                        }
                                        //Sinon on return false
                                        else {
                                            res.send(false);
                                        }
                                    });
                    }
                    //Si pas de boule a coté on déplace juste le joueur
                    else {
                        makeStardogRequest(`
                            DELETE { ?player :isOn ?oldCell . }
                            INSERT { ?player :isOn ?newCell . }
                            WHERE {
                                ?player rdf:type :Player .
                                ?oldCell rdf:type :CellPlayer .
                                ?oldCell `+ move + ` ?newCell .
                            }`
                            , function (data) {
                                makeStardogRequest(specialCellsReq, function (data) {
                                    res.send(data.results.bindings);
                                });
                            });
                    }
                });
        }
        else {
            res.send(false);
        }
    });
    console.log();
})


app.listen(8085, function () {
    console.log('Listening on port 8085!')
})