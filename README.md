# M2_Snowman

Pour lancer le client :

    cd client_vue
    npm install
    npm run serve

Pour lancer le serveur :

    cd server
    npm install
    npm start

L'application nécessite une base de connaissance dont le code est présent dans src.ttl pour fonctionner

Il peut être nécessaire de changer l'adresse de l'api pour le client (dans client_vue/src/store/index.js variable url)
Il peut être nécessaire de changer l'adresse du serveur Stardog pour l'API (dans server/server.js conn.endpoint)